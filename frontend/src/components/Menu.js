import React from 'react';
import { Link } from 'react-router-dom';
import "../styles/components/Menu.css"

const Menu = () => {
    return (
        <div id="menuComponent">
            <menu>
                <li id="solutecImage">
                </li>
                <Link to="/" id="home" exact data-testid="home" className='menuLink'>
                    <h1>Accueil</h1>
                </Link>


                <Link to="/analysis" exact id="analysis" data-testid="analysis" className='menuLink' >
                    <h1>Analyse</h1>
                </Link>


                <Link to="/history" exact id="history" data-testid="history" className='menuLink'>
                    <h1>Historique</h1>
                </Link>


                <Link to="/search" exact id="search" data-testid="search" className='menuLink'>
                    <h1>Recherche</h1>
                </Link>


                <Link to="/login" exact id="logout" data-testid="logout" className='menuLink'>
                    <h1>Déconnexion</h1>
                </Link>

            </menu>
        </div>
    );
};

export default Menu
import { render, screen, fireEvent } from "@testing-library/react";
import { toBeInTheDocument } from "@testing-library/jest-dom"
import Menu from "../../components/Menu.js";
import React from "react";
import { BrowserRouter, MemoryRouter, Route, Routes } from "react-router-dom";
import Home from "./../../pages/Home.js"
import Analysis from "./../../pages/Analysis.js"
import History from "./../../pages/History.js";
import Search from "./../../pages/Search.js";
import Login from "./../../pages/Login.js"




describe('Menu', () => {
    test("Check if Home Link is mounted", () => {

        render(<BrowserRouter> <Menu></Menu></BrowserRouter >,);


        const homeElement = screen.getByTestId("home");
        expect(homeElement).toBeInTheDocument();
    });

    test("Check if Analysis Link is mounted", () => {

        render(<BrowserRouter><Menu></Menu></BrowserRouter>,);


        const homeElement = screen.getByTestId("analysis");
        expect(homeElement).toBeInTheDocument();
    });
    test("Check if History Link is mounted", () => {

        render(<BrowserRouter><Menu></Menu></BrowserRouter>,);


        const homeElement = screen.getByTestId("history");
        expect(homeElement).toBeInTheDocument();
    })

    test("Check if Search Link is mounted", () => {

        render(<BrowserRouter><Menu></Menu></BrowserRouter>,);


        const homeElement = screen.getByTestId("search");
        expect(homeElement).toBeInTheDocument();
    })

    test("Check if Logout Link is mounted", () => {

        render(<BrowserRouter><Menu></Menu></BrowserRouter>,);


        const homeElement = screen.getByTestId("logout");
        expect(homeElement).toBeInTheDocument();
    });
    test('Clicking home link should redirect to home page', () => {
        render(
            <MemoryRouter initialEntries={['/analysis']}>
                <Routes>
                    <Route path="/analysis" element={<Menu />}></Route>
                    <Route path="*" element={<Home />}></Route>
                </Routes>
            </MemoryRouter>
        );

        fireEvent.click(screen.getByTestId('home'));

        expect(screen.getByTestId('home-page')).toBeInTheDocument();
    });

    test("Checks if the link Analysis redirects to the analysis page", () => {

        render(
            <MemoryRouter initialEntries={['/']}>
                <Routes>
                    <Route path="/analysis" element={<Analysis />}></Route>
                    <Route path="*" element={<Menu />}></Route>
                </Routes>
            </MemoryRouter>
        );

        fireEvent.click(screen.getByTestId('analysis'));

        expect(screen.getByTestId('analysis-page')).toBeInTheDocument();
    });
    test("Checks if the link History redirects to the history page", () => {
        render(
            <MemoryRouter initialEntries={['/']}>
                <Routes>
                    <Route path="/history" element={<History />}></Route>
                    <Route path="*" element={<Menu />}></Route>
                </Routes>
            </MemoryRouter>
        );

        fireEvent.click(screen.getByTestId('history'));

        expect(screen.getByTestId('history-page')).toBeInTheDocument();
    });
    test("Checks if the link Search redirects to the searching page", () => {


        render(
            <MemoryRouter initialEntries={['/']}>
                <Routes>
                    <Route path="/search" element={<Search />}></Route>
                    <Route path="*" element={<Menu />}></Route>
                </Routes>
            </MemoryRouter>
        );

        fireEvent.click(screen.getByTestId('search'));
        expect(screen.getByTestId('search-page')).toBeInTheDocument();
    });
    test("Checks if the link Logout redirects to the login page", () => {


        render(
            <MemoryRouter initialEntries={['/']}>
                <Routes>
                    <Route path="/login" element={<Login />}></Route>
                    <Route path="*" element={<Menu />}></Route>
                </Routes>
            </MemoryRouter>
        );

        fireEvent.click(screen.getByTestId('logout'));
        expect(screen.getByTestId('login-page')).toBeInTheDocument();
    })
});
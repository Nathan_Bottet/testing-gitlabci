import { BrowserRouter as Router, Routes, Route, } from "react-router-dom"
import Home from "./pages/Home.js"
import Analysis from "./pages/Analysis.js"
import History from "./pages/History.js";
import Search from "./pages/Search.js";
import Login from "./pages/Login.js"


function App() {
  return (
    <Router>
      <Routes>

        <Route path="/analysis" element={<Analysis />}></Route>
        <Route path="/history" element={<History />}></Route>
        <Route path="/search" element={<Search />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="*" element={<Home />}></Route>

      </Routes>
    </Router>
  );
}

export default App;

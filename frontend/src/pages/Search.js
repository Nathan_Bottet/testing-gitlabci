import React from 'react';
import Menu from "./../components/Menu.js"

const Search = () => {
    return (
        <>
            <div data-testid="search-page">
                <Menu />
                <h1>Search</h1>
            </div>
        </>
    );
};

export default Search;
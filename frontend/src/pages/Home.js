import React from 'react';
import Menu from "./../components/Menu.js"
import "../styles/pages/Home.css"

const Home = () => {
    return (
        <>
            <div data-testid="home-page">
                <Menu />
            </div>
        </>
    );
};

export default Home;    
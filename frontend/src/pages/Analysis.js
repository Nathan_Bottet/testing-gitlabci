import React from 'react';
import Menu from "./../components/Menu.js"


const Analysis = () => {
    return (
        <>
            <div data-testid="analysis-page">
                <Menu />
                <h1>Analysis</h1>
            </div>
        </>
    );
};

export default Analysis;
import React from 'react';
import Menu from "./../components/Menu.js"

const Login = () => {
    return (
        <>
            <div data-testid="login-page">
                <Menu />
                <h1>Login</h1>
            </div>
        </>
    );
};

export default Login;
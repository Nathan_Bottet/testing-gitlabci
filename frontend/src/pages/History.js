import React from 'react';
import Menu from "./../components/Menu.js"

const History = () => {
    return (
        <>
            <div data-testid="history-page">
                <Menu />
                <h1>History</h1>
            </div>
        </>
    );
};

export default History;